package com.vitalyk.raytracer

import org.joml.Vector3d
import org.joml.Vector4d

data class Vector3 (
    val x: Double = 0.0,
    val y: Double = 0.0,
    val z: Double = 0.0
) {

    companion object {
        val zero = Vector3()

        val xUnit = Vector3(1.0, 0.0, 0.0)
        val yUnit = Vector3(0.0, 1.0, 0.0)
        val zUnit = Vector3(0.0, 0.0, 1.0)
    }

    val isZero: Boolean
        get() = x == 0.0 && y == 0.0 && z == 0.0

    operator fun unaryMinus() = Vector3(-x, -y, -z)
    operator fun plus(other: Vector3) = Vector3(x + other.x, y + other.y, z + other.z)
    operator fun minus(other: Vector3) = Vector3(x - other.x, y - other.y, z - other.z)
    operator fun times(k: Double) = Vector3(x * k, y * k, z * k)

    /**
     * Replaces the components of this vector with the given ones.
     */
    fun change(x: Double = this.x, y: Double = this.y, z: Double = this.z) = Vector3(x, y, z)

    /**
     * Increments the components of this vector by the given amounts.
     */
    fun shift(x: Double = 0.0, y: Double = 0.0, z: Double = 0.0) =
        Vector3(this.x + x, this.y + y, this.z + z)

    /**
     * The dot product: |a| x [b| * cos(θ).
     */
    fun dot(other: Vector3): Double = x * other.x + y * other.y + z * other.z

    fun norm(): Double = x * x + y * y + z * z

    /**
     * Use isZero property instead of the expensive vec.length() == 0 check.
     */
    fun length(): Double = norm().sqrt()

    fun normalize(): Vector3 = this * (1.0 / length())

    /**
     * Returns a vector that is perpendicular to this vector and the given vector.
     */
    fun cross(other: Vector3) = Vector3(
        y * other.z - z * other.y,
        z * other.x - x * other.z,
        x * other.y - y * other.x
    )
}

fun Vector3.toVector4d() = Vector4d(x, y, z, 1.0)
fun Vector4d.toVector3() = Vector3(x, y, z)
fun Vector3.toVector3d() = Vector3d(x, y, z)
fun Vector3d.toVector3() = Vector3(x, y, z)