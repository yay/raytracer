package com.vitalyk.raytracer

import com.vitalyk.raytracer.light.Light
import com.vitalyk.raytracer.shape.Shape

/**
 * A scene in the right-handed coordinate system.
 * Contains shapes, lights and a camera.
 */
class Scene (
    var camera: Camera = Camera(),
    var items: List<Shape> = emptyList(),
    var lights: List<Light> = emptyList()
) {
    /**
     * Find the Shape instance that the ray intersects.
     * If multiple shapes are intersected, pick the closest one.
     */
    fun trace(ray: Ray): Intersection? {
        var minDistance = Double.MAX_VALUE
        var closestItem: Shape? = null
        val ln = items.size

        for (i in 0 until ln) {

            val item = items[i]
            val distance = item.intersect(ray)

            if (distance != null && distance < minDistance) {
                minDistance = distance
                closestItem = item
            }
        }

        return if (closestItem != null)
            Intersection(minDistance, closestItem)
        else
            null
    }
}