package com.vitalyk.raytracer

import java.beans.PropertyChangeEvent
import java.beans.PropertyChangeListener

class FpsCounter {
    var fps: Int = 0
        private set(value) {
            field = value
        }

    private var oldFps: Int = 0
    private var start: Long = System.nanoTime()
    private var frameCounter: Int = 0

    private val listeners = mutableListOf<PropertyChangeListener>()

    fun countFrame() {
        val now = System.nanoTime()
        val secondsSinceStart: Long = (now - start) / 1_000_000_000

        fps++
        frameCounter++

        if (secondsSinceStart > 1) {
            notifyListeners(fps)
            fps = 0
            start = now
        }
    }

    fun addListener(listener: PropertyChangeListener) {
        listeners.add(listener)
    }

    fun removeListener(listener: PropertyChangeListener) {
        listeners.remove(listener)
    }

    private fun notifyListeners(fps: Int) {
        for (listener in listeners) {
            listener.propertyChange(PropertyChangeEvent(this, "fps", oldFps, fps))
        }
        oldFps = fps
    }
}