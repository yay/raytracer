package com.vitalyk.raytracer.light

import com.vitalyk.raytracer.Color
import com.vitalyk.raytracer.Vector3

class Spherical(
    override var color: Color,
    var intensity: Double,
    var position: Vector3
) : Light() {
    override fun distanceToPoint(hitPoint: Vector3) = (position - hitPoint).length()
    override fun directionToLight(hitPoint: Vector3) = (position - hitPoint).normalize()
    override fun intensityAtPoint(hitPoint: Vector3) =
        intensity / (4 * Math.PI * (position - hitPoint).norm())
}