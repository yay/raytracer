package com.vitalyk.raytracer.light

import com.vitalyk.raytracer.Color
import com.vitalyk.raytracer.Vector3

/**
 * Directional light approximate light from the sun or stars - objects so far away
 * that their light rays are effectively parallel to each other
 * and at an approximately-constant intensity level.
 */
class Directional(
    override var color: Color,
    var intensity: Double,
    direction: Vector3
) : Light() {
    /**
     * Since light direction doesn't change no matter what part of the shape is hit,
     * we can optimize by normalizing the given direction once it's set.
     */
    var direction = direction.normalize()
        set(value) { field = value.normalize() }

    override fun distanceToPoint(hitPoint: Vector3) = Double.POSITIVE_INFINITY
    override fun directionToLight(hitPoint: Vector3) = -direction
    override fun intensityAtPoint(hitPoint: Vector3) = intensity
}