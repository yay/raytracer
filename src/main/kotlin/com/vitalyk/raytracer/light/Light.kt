package com.vitalyk.raytracer.light

import com.vitalyk.raytracer.Color
import com.vitalyk.raytracer.Vector3

abstract class Light {
    abstract var color: Color

    abstract fun distanceToPoint(hitPoint: Vector3): Double
    abstract fun directionToLight(hitPoint: Vector3): Vector3
    abstract fun intensityAtPoint(hitPoint: Vector3): Double
}