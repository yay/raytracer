package com.vitalyk.raytracer

import com.vitalyk.raytracer.view.MainView
import javafx.application.Application
import tornadofx.*

/**
 * See https://bheisler.github.io/post/writing-raytracer-in-rust-part-1/
 */

class RaytracerApp : App(MainView::class)

fun main(args: Array<String>) {
    Application.launch(RaytracerApp::class.java, *args)
}