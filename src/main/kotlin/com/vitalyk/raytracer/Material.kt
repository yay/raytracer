package com.vitalyk.raytracer

data class Material(
    val color: Color = Color(1.0, 1.0, 1.0),
    val texture: Texture? = null,
    /**
     * Specifies how much light energy is reflected by a shape and how much is absorbed.
     */
    val albedo: Float = 0.3f,
    /**
     * Specifies how much of the final color comes from the reflection.
     */
    val reflectivity: Float = 0.0f,
    val refraction: Float = 0.0f,
    val transparency: Float = 0.0f
) {
    fun color(uv: UV?): Color {
        if (uv != null && texture != null) {
            return texture.colorAt(uv)
        }
        return color
    }
}