package com.vitalyk.raytracer.view

import com.vitalyk.raytracer.*
import com.vitalyk.raytracer.shape.Sphere
import javafx.animation.AnimationTimer
import javafx.beans.property.SimpleBooleanProperty
import javafx.geometry.Insets
import javafx.geometry.Orientation
import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.canvas.Canvas
import javafx.scene.control.Alert
import javafx.scene.control.Label
import javafx.scene.control.ScrollPane
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyEvent
import javafx.scene.layout.Priority
import javafx.scene.layout.VBox
import tornadofx.*
import java.beans.PropertyChangeListener

class MainView : View() {
    override val root = VBox()

    private val referenceWidth = 800
    private val referenceHeight = 600

    private var renderWidth: Int = referenceWidth
        set(value) {
            field = value
            updateRenderSize()
        }

    private var renderHeight: Int = referenceHeight
        set(value) {
            field = value
            updateRenderSize()
        }

    private lateinit var mainPane: ScrollPane
    private val currentScene: Scene = getTestScene()
    private var renderMatrix = ColorMatrix(renderWidth, renderHeight)
    private val renderCanvas: Canvas = Canvas(referenceWidth, referenceHeight)

    private val fpsCounter = FpsCounter()

    private fun updateRenderSize() {
        val camera = currentScene.camera

        camera.width = renderWidth
        camera.height = renderHeight
        camera.updateFov()

        renderMatrix = ColorMatrix(renderWidth, renderHeight)
    }

    private fun render() {
        currentScene.camera.renderScene(currentScene, renderMatrix)
        fpsCounter.countFrame()

        renderMatrix.fillCanvas(renderCanvas, scale = true)
        mainPane.content = renderCanvas
    }

    private fun fxaa() {
        if (mainPane.content is Canvas) {
            val image = mainPane.content.snapshot(null, null)

            val vbox = VBox()
            vbox.children.addAll(
                fxaaImage(image.toColorMatrix()).toImage().toCanvas(),
                getImageLuma(image.toColorMatrix()).toImage().toCanvas(),
                fxaaImage(image.toColorMatrix(), FxaaDebug.Passthrough).toImage().toCanvas(),
                fxaaImage(image.toColorMatrix(), FxaaDebug.HorzVert).toImage().toCanvas()
            )
            mainPane.content = vbox
        }
    }

    var animationTimer: AnimationTimer? = null

    private fun startAnimation() {
        val movingSphere = currentScene.items[2] as Sphere
        val centerSphere = currentScene.items[0] as Sphere
        val cx = centerSphere.center.x + 1.0
        val cz = centerSphere.center.z
        val r = 4.0
        val speed = 60

        animationTimer = object : AnimationTimer() {
            override fun handle(now: Long) {
                val seconds = now / 1_000_000_000.0
                val degrees = seconds * speed % 360.0
                val radians = degrees / 180.0 * Math.PI
                val x = cx + r * Math.cos(radians)
                val z = cz + r * Math.sin(radians)
                movingSphere.center = movingSphere.center.change(x = x, z = z)
                render()
            }
        }

        animationTimer?.start()
    }

    private fun stopAnimation() {
        animationTimer?.stop()
        animationTimer = null
    }

    private fun grayscale() {
        if (mainPane.content is Canvas) {
            val image = mainPane.content.snapshot(null, null)
            val pixels = image.toColorMatrix()

            val vbox = VBox()
            vbox.children.addAll(
                Label("Lightness"),
                pixels.recolor {
                    color -> color.getLightness().toColor()
                }.toImage().toCanvas(),

                Label("Average"),
                pixels.recolor {
                    color -> color.getAverage().toColor()
                }.toImage().toCanvas(),

                Label("Luma"),
                pixels.recolor {
                    color -> color.getLuma().toColor()
                }.toImage().toCanvas()
            )
            mainPane.content = vbox
        } else {
            val alert = Alert(Alert.AlertType.INFORMATION, "The main node is not a Canvas.\n" +
                "Render a scene or load an image first.")
            alert.headerText = "No canvas found"
            alert.showAndWait()
        }
    }

    private fun loadImage() {
        mainPane.content = loadImageDialog(primaryStage)?.toCanvas() ?: mainPane.content
    }

    private fun saveImage(canvas: Node) {
        if (canvas is Canvas) {
            canvas.snapshot(null, null).saveDialog(
                owner = primaryStage,
                fileName = "Raytracing.png"
            )
        }
    }

    private fun setupCanvas(canvas: Canvas) {
        canvas.contextmenu {
            item("Save Image...").action {
                saveImage(canvas)
            }
        }

        var startX = 0.0
        var startY = 0.0

        fun lookAt(dx: Double, dy: Double) {
            // Assuming the drag started in the middle of the view, the maximum
            // horizontal displacement can be half width and the maximum vertical
            // half height. We assume that such displacements equal a 90 degree
            // turn horizontally / vertically.
            // So the formula essentially is:
            // angle = -dx / (0.5 * width * 0.5 * PI)
            // We can also introduce some factor to act as mouse sensitivity.

            val quarterPi = 0.25 * Math.PI
            val sensitivity = 1.0
            val xAngle = sensitivity * -dy / (quarterPi * renderHeight)
            val yAngle = sensitivity * -dx / (quarterPi * renderWidth)
        }

        canvas.setOnMousePressed { event ->
            startX = event.screenX
            startY = event.screenY
        }
        canvas.setOnMouseDragged { event ->
            if (event.isPrimaryButtonDown) {
                val dx = event.screenX - startX
                val dy = event.screenY - startY
                lookAt(dx, dy)
                render()
            }
        }
    }

    private fun showGammaView() {
        find(GammaView::class).openWindow(owner = null)
    }

    init {
        with (primaryStage) {
            title = "Raytracer"
            minWidth = referenceWidth.toDouble()
            minHeight = referenceHeight.toDouble()
            shortcut("Shortcut+S") {
                saveImage(mainPane.content)
            }
        }

        val wPressed = SimpleBooleanProperty(false)
        val sPressed = SimpleBooleanProperty(false)
        val aPressed = SimpleBooleanProperty(false)
        val dPressed = SimpleBooleanProperty(false)

        fun moveCamera() {
            val step = 0.5
            val camera = currentScene.camera
            val direction = camera.direction
            val forward = direction.change(y = 0.0).normalize() * step
            val sideways = -camera.up.cross(direction).change(y = 0.0).normalize() * step
            var p = camera.origin

            if (wPressed.get()) {
                p += forward
            }
            if (sPressed.get()) {
                p -= forward
            }
            if (aPressed.get()) {
                p -= sideways
            }
            if (dPressed.get()) {
                p += sideways
            }

            if (p != camera.origin) {
                camera.origin = p
                render()
            }
        }

        primaryStage.addEventFilter(KeyEvent.KEY_PRESSED) { event ->
            when (event.code) {
                KeyCode.W -> wPressed.set(true)
                KeyCode.S -> sPressed.set(true)
                KeyCode.A -> aPressed.set(true)
                KeyCode.D -> dPressed.set(true)
                else -> {}
            }

            moveCamera()
        }

        primaryStage.addEventFilter(KeyEvent.KEY_RELEASED) { event ->
            when (event.code) {
                KeyCode.W -> wPressed.set(false)
                KeyCode.S -> sPressed.set(false)
                KeyCode.A -> aPressed.set(false)
                KeyCode.D -> dPressed.set(false)
                else -> {}
            }
        }

        setupCanvas(renderCanvas)

        val mainMenu = menubar {
            menu("File") {
                item("Render").action {
                    render()
                }
                item("FXAA").action {
                    fxaa()
                }
                item("Gamma").action {
                    showGammaView()
                }
            }
        }

        if (System.getProperty("os.name").startsWith("Mac")) {
            mainMenu.isUseSystemMenuBar = true
        }

        with (root) {
            hbox {
                spacing = 10.0
                padding = Insets(10.0)
                alignment = Pos.CENTER_LEFT

                button("Render") {
                    setOnAction {
                        currentScene.camera.fov = 90.0
                        render()
                    }
                }
                togglebutton("Half Resolution") {
                    isSelected = false
                    action {
                        val factor: Double
                        if (isSelected) {
                            factor = 0.5
                            text = "Full Resolution"
                        } else {
                            factor = 1.0
                            text = "Half Resolution"
                        }
                        renderWidth = (referenceWidth * factor).toInt()
                        renderHeight = (referenceHeight * factor).toInt()
                        render()
                    }
                }
                button("FXAA") {
                    tooltip("Applies Fast Approximate Anti-Aliasing to the rendered\n" +
                        "or loaded image, showing the key steps of the process.")
                    setOnAction {
                        fxaa()
                    }
                }
                button("Grayscale") {
                    tooltip("Produces a grayscale image by averaging\n" +
                        "individual color components in various ways.")
                    setOnAction {
                        grayscale()
                    }
                }
                button("Load image...") {
                    setOnAction {
                        loadImage()
                    }
                }
                button("Gamma") {
                    setOnAction {
                        showGammaView()
                    }
                }
                button("Colors").action {
                    find(ColorView::class).openWindow(owner = null)
                }
                togglebutton("Animation") {
                    isSelected = false
                    setOnAction {
                        if (animationTimer == null) {
                            startAnimation()
                        } else {
                            stopAnimation()
                        }
                    }
                }

                val fpsLabel = label("FPS: 0")
                fpsCounter.addListener(PropertyChangeListener { event ->
                    fpsLabel.text = "FPS: ${event.newValue as Int}"
                })
            }
            hbox {
                spacing = 10.0
                padding = Insets(10.0)
                alignment = Pos.CENTER_LEFT

                label("FOV:")
                slider(40.0, 140.0, 90.0, Orientation.HORIZONTAL) {
                    isShowTickMarks = true
                    isShowTickLabels = true
                    valueProperty().addListener { _, _, newValue ->
                        currentScene.camera.fov = newValue.toDouble()
                        render()
                    }
                }

                button("Save scene...") {
                    isVisible = false
                    setOnAction {
                        Settings.saveDialog(obj = currentScene, owner = primaryStage)
                    }
                }
            }
            mainPane = scrollpane {
                vgrow = Priority.ALWAYS
            }
        }

        updateRenderSize()
        setupCanvas(renderCanvas)
        render()
    }
}
