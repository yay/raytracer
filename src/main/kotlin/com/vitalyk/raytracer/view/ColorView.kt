package com.vitalyk.raytracer.view

import javafx.scene.layout.Priority
import javafx.scene.layout.VBox
import tornadofx.*

class ColorView : View("Solid Colors") {
    override val root = VBox().apply {
        vgrow = Priority.ALWAYS

        contextmenu {
            item("Red").action { setColor("#f00") }
            item("Green").action { setColor("#0f0") }
            item("Blue").action { setColor("#00f") }
            item("White").action { setColor("#fff") }
            item("Black").action { setColor("#000") }
        }
    }

    fun setColor(color: String) {
        root.style {
            backgroundColor += c(color)
        }
    }
}