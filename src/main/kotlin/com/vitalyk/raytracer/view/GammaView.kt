package com.vitalyk.raytracer.view

import com.vitalyk.raytracer.*
import javafx.scene.layout.VBox
import tornadofx.*

// http://blog.johnnovak.net/2016/09/21/what-every-coder-should-know-about-gamma/

class GammaView : View() {
    override val root = object : VBox() {

        init {
            minWidth = 800.0
            minHeight = 800.0
        }

        override fun resize(width: Double, height: Double) {
            super.resize(width, height)

            if (width <= 0) return

            val w = width.toInt()
            val h = if (height > 1000) 70 else 50

            // Gamma of 2.2 approximately matches the power law sensitivity of human vision.
            // encodedValue = linearValue ^ (1/gamma)
            // linearValue = encodedValue ^ gamma

            val encodeGamma = 1/2.2
            val decodeGamma = 2.2

            val bwGradientSimpleGamma = ColorMatrix(w, h).fill(chunks = 8) { x, _ ->
                val c = (x / w.toDouble()).pow(encodeGamma)
                Color(c, c, c)
            }

            val bwGradientProperGamma = ColorMatrix(w, h).fill(chunks = 8) { x, _ ->
                val c = x / w.toDouble()
                Color(c, c, c).sRGB()
            }

            val bwGradientNoGamma = ColorMatrix(w, h).fill(chunks = 8) { x, _ ->
                val c = x / w.toDouble()
                Color(c, c, c)
            }

            val red = FxColor.RED.toColor()
            val lime = FxColor.LIME.toColor()
            val blue = FxColor.BLUE.toColor()
            val fuchsia = FxColor.FUCHSIA.toColor()
            val yellow = FxColor.YELLOW.toColor()
            val lightBlue = FxColor.LIGHTBLUE.toColor()
            val limeGreen = FxColor.LIMEGREEN.toColor()

            fun makeGradient(from: Color, to: Color, pow: Double = 1.0): ColorMatrix {
                return ColorMatrix(w, h).fill(chunks = 8) { x, _ ->
                    val t = x / w.toDouble()
                    val r = (from.r * (1 - t) + to.r * t).pow(pow)
                    val g = (from.g * (1 - t) + to.g * t).pow(pow)
                    val b = (from.b * (1 - t) + to.b * t).pow(pow)
                    Color(r, g, b)
                }
            }

            fun makeProperGradient(from: Color, to: Color): ColorMatrix {
                return ColorMatrix(w, h).fill(chunks = 8) { x, _ ->
                    val t = x / w.toDouble()
                    val r = from.r * (1 - t) + to.r * t
                    val g = from.g * (1 - t) + to.g * t
                    val b = from.b * (1 - t) + to.b * t
                    Color(r, g, b).sRGB()
                }
            }

            // JavaFx Color (aliased as FxColor here) is in sRGB color space,
            // so we convert Fx colors to linear space first before calculating
            // gradients that we are about to gamma correct.
            val redLime_simple = makeGradient(red.linear(), lime.linear(), encodeGamma)
            val redLime_sRGB = makeProperGradient(red.linear(), lime.linear())
            // This gradient is calculated in sRGB gamma space directly
            // and no gamma correction is applied.
            val redLime_uncorrected = makeGradient(red, lime)

            val limeBlue_sRGB = makeProperGradient(lime.linear(), blue.linear())
            val limeBlue_uncorrected = makeGradient(lime, blue)

            val blueRed_sRGB = makeProperGradient(blue.linear(), red.linear())
            val blueRed_uncorrected = makeGradient(blue, red)

            val fuchsiaYellow_sRGB = makeProperGradient(fuchsia.linear(), yellow.linear())
            val fuchsiaYellow_uncorrected = makeGradient(fuchsia, yellow)

            children.clear()

            label("Light intensity grows linearly physically (simple gamma encoding)")
            this += bwGradientSimpleGamma.toCanvas()
            label("Light intensity grows linearly physically (more accurate sRGB gamma encoding)")
            this += bwGradientProperGamma.toCanvas()
            label("Light intensity grows linearly perceptually (no gamma encoding)")
            this += bwGradientNoGamma.toCanvas()
            label {
                minHeight = 30.0
            }

            label("Linear gradient with simple gamma encoding (real-life color mixing)")
            this += redLime_simple.toCanvas()
            label("Linear gradient with sRGB gamma encoding (real-life color mixing)")
            this += redLime_sRGB.toCanvas()
            label("Linear gradient with no gamma encoding (how colors mix in most software, including CSS gradients)")
            this += redLime_uncorrected.toCanvas()
            label("Pairs of linear gradients below have sRGB (top) an no (bottom) gamma encoding") {
                minHeight = 30.0
            }

            this += limeBlue_sRGB.toCanvas()
            this += limeBlue_uncorrected.toCanvas()
            label { minHeight = 10.0 }

            this += blueRed_sRGB.toCanvas()
            this += blueRed_uncorrected.toCanvas()
            label { minHeight = 10.0 }

            this += fuchsiaYellow_sRGB.toCanvas()
            this += fuchsiaYellow_uncorrected.toCanvas()
        }
    }
}

