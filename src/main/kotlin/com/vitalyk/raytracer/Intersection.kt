package com.vitalyk.raytracer

import com.vitalyk.raytracer.shape.Shape

data class Intersection (
    var distance: Double,
    var target: Shape
)