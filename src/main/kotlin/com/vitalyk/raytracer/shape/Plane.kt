package com.vitalyk.raytracer.shape

import com.vitalyk.raytracer.Vector3
import com.vitalyk.raytracer.Ray
import com.vitalyk.raytracer.UV
import com.vitalyk.raytracer.Material

/**
 * Represents an infinite plane.
 */
class Plane(
    var origin: Vector3,
    normal: Vector3,
    override var material: Material
) : Shape() {

    var normal = normal.normalize()
        set(value) {
            field = value.normalize()
            updateAxes()
        }

    lateinit var xAxis: Vector3
    lateinit var yAxis: Vector3

    init {
        updateAxes()
    }

    fun updateAxes() {
        // Construct a coordinate system aligned with the plane.
        // Cross product of two vectors gives us a vector
        // that is perpendicular to them.
        xAxis = normal.cross(Vector3.zUnit)

        // Normal and the z-axis are coincident. Use the y-axis instead.
        if (xAxis.isZero) {
            xAxis = normal.cross(Vector3.yUnit)
        }

        yAxis = normal.cross(xAxis)
    }

    override fun intersect(ray: Ray): Double? {
        val denom = normal.dot(ray.direction)

        if (denom > 1e-6) {
            val v = origin - ray.origin
            val distance = v.dot(normal) / denom
            if (distance >= 0.0f) {
                return distance
            }
        }

        return null
    }

    override fun normal(hitPoint: Vector3): Vector3 = -normal

    override fun uv(hitPoint: Vector3): UV {
        val hitVector = hitPoint - origin

        // It might be useful to add a scaling factor and an offset
        // to allow the user to adjust the position and size of the texture.

        return UV(
            hitVector.dot(xAxis),
            hitVector.dot(yAxis)
        )
    }
}