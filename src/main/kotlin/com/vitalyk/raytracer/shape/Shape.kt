package com.vitalyk.raytracer.shape

import com.vitalyk.raytracer.Material

abstract class Shape : Intersectable {
    val id: String = ""
    abstract var material: Material
}