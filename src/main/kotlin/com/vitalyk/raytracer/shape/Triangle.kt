package com.vitalyk.raytracer.shape

import com.vitalyk.raytracer.Vector3
import com.vitalyk.raytracer.Material
import com.vitalyk.raytracer.UV
import com.vitalyk.raytracer.Ray

class Triangle(
    override var material: Material
) : Shape() {

    override fun intersect(ray: Ray): Double? {
        return null
    }

    override fun normal(hitPoint: Vector3): Vector3 {
        return Vector3()
    }

    override fun uv(hitPoint: Vector3): UV {
        return UV()
    }
}