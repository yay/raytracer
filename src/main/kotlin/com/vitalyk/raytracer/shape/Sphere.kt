package com.vitalyk.raytracer.shape

import com.vitalyk.raytracer.*
import com.vitalyk.raytracer.math.Icecore
import com.vitalyk.raytracer.sqrt
import org.apache.commons.math3.util.FastMath

class Sphere(
    var center: Vector3,
    var radius: Double,
    override var material: Material
) : Shape() {

    override fun intersect(ray: Ray): Double? {
        // Create a line segment between the ray origin and the center of the sphere.
        val h: Vector3 = center - ray.origin
        // Use h as a hypotenuse and find the length of the adjacent side.
        val adj = h.dot(ray.direction) // adjacent side
        // Find the length-squared of the opposite side.
        // h.dot(h) is equivalent to (but faster than): h.length() * h.length().
        val opp2 = h.dot(h) - (adj * adj)
        val radius2 = radius * radius

        // If length-squared is less than radius-squared, the ray intersects the sphere.
        if (opp2 > radius2) {
            return null
        }

        val halfChord = (radius2 - opp2).sqrt()
        val t0 = adj - halfChord
        val t1 = adj + halfChord

        if (t0 < 0.0 && t1 < 0.0) {
            return null
        }

        return if (t0 < t1) t0 else t1
    }

    override fun normal(hitPoint: Vector3): Vector3 = (hitPoint - center).normalize()

    override fun uv(hitPoint: Vector3): UV {
        // Sphere surface coordinates relative to its center.
        val hitVector = hitPoint - center

        val phi = Icecore.atan2(hitVector.z, hitVector.x)  // [-pi..pi]
        val theta = FastMath.acos(hitVector.y / radius)    //   [0..pi]

        return UV(
            u = (1.0 + phi / Math.PI) * 0.5, // [0..1]
            v = theta / Math.PI              // [0..1]
        )
    }
}