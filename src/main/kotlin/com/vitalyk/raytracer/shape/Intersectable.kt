package com.vitalyk.raytracer.shape

import com.vitalyk.raytracer.Vector3
import com.vitalyk.raytracer.Ray
import com.vitalyk.raytracer.UV

interface Intersectable {
    /**
     * Returns the distance to the shape the ray intersects.
     */
    fun intersect(ray: Ray): Double?

    /**
     * Returns the surface normal at the hit point.
     * For example, given the direction and intensity of light,
     * we can calculate the amount of light the point receives
     * via the Lambert's Cosine Law.
     */
    fun normal(hitPoint: Vector3): Vector3

    /**
     * Returns texture coordinates at the hit point.
     */
    fun uv(hitPoint: Vector3): UV
}