package com.vitalyk.raytracer

import java.util.concurrent.Callable
import java.util.concurrent.ForkJoinPool

typealias Matrix2D<T> = Array<Array<T>>

inline fun <reified T> Matrix2D(w: Int, h: Int, value: T) =
    Array(w) { Array(h) { value } }

// https://stackoverflow.com/questions/45439765/multi-threaded-array-initialization-in-kotlin

inline fun <reified T> Matrix2D(w: Int, h: Int, threaded: Boolean = false, crossinline init: (Int, Int) -> T): Matrix2D<T> =
    if (threaded)
        generatorOf(w) { x -> Array(h) { y -> init(x, y) } }.let { Array(w) { x -> it[x]() } }
    else
        Array(w) { x -> Array(h) { y -> init(x, y) } }

//                          v--- creates array of generator functions
inline fun <reified T> generatorOf(size: Int, crossinline init: (Int) -> T): Array<() -> T> =
    ForkJoinPool.commonPool().let {  //   get array from ForkJoinTask ---v
        Array(size) { i -> it.submit( Callable { init(i) } ).let { { it.get() } } }
//                         return the lambda generator () -> T  ---^
    }

class DoubleMatrix2D(val width: Int, val height: Int, init: ((Int, Int) -> Double)? = null) {

    private val data: DoubleArray

    init {
        data = if (init != null) {
            DoubleArray(width * height) { i -> init(i % width, i / width) }
        } else {
            DoubleArray(width * height)
        }
    }

    @Suppress("NOTHING_TO_INLINE")
    inline fun inBounds(x: Int, y: Int): Boolean = x > -1 && x < width && y > -1 && y < height

    operator fun get(x: Int, y: Int): Double {
        if (inBounds(x, y)) {
            return data[y * width + x]
        } else {
            throw ArrayIndexOutOfBoundsException()
        }
    }

    operator fun set(x: Int, y: Int, value: Double): Unit {
        if (inBounds(x, y)) {
            data[y * width + x] = value
        } else {
            throw ArrayIndexOutOfBoundsException()
        }
    }
}

data class Size2D<out T>(
    val width: T,
    val height: T
)

fun <T> Matrix2D<T>.size(): Size2D<Int> = if (size > 0) Size2D(size, first().size) else Size2D(0, 0)

fun Double.toRadians() = this / 180 * Math.PI
fun Double.format(digits: Int): String = java.lang.String.format("%.${digits}f", this)

@Suppress("NOTHING_TO_INLINE")
inline fun Double.sqrt() = StrictMath.sqrt(this)
// There's no 'sqrt' for floats.

@Suppress("NOTHING_TO_INLINE")
inline fun Double.pow(p: Double) = StrictMath.pow(this, p)

@Suppress("NOTHING_TO_INLINE")
inline fun Double.min(other: Double) = Math.min(this, other)
@Suppress("NOTHING_TO_INLINE")
inline fun Float.min(other: Float) = Math.min(this, other)

@Suppress("NOTHING_TO_INLINE")
inline fun Double.max(other: Double) = Math.max(this, other)
@Suppress("NOTHING_TO_INLINE")
inline fun Float.max(other: Float) = Math.max(this, other)

@Suppress("NOTHING_TO_INLINE")
inline fun Double.clamp(min: Double, max: Double) = this.max(min).min(max)
@Suppress("NOTHING_TO_INLINE")
inline fun Float.clamp(min: Float, max: Float) = this.max(min).min(max)

@Suppress("NOTHING_TO_INLINE")
inline fun <T> Comparable<T>.clamp(min: T, max: T): T {
    @Suppress("UNCHECKED_CAST")
    return if (this < min) min else if (this > max) max else this as T
}

@Suppress("NOTHING_TO_INLINE")
inline fun Double.clampMin(min: Double) = this.max(min)
@Suppress("NOTHING_TO_INLINE")
inline fun Float.clampMin(min: Float) = this.max(min)

@Suppress("NOTHING_TO_INLINE")
inline fun Double.clampMax(max: Double) = this.min(max)
@Suppress("NOTHING_TO_INLINE")
inline fun Float.clampMax(max: Float) = this.min(max)

@Suppress("NOTHING_TO_INLINE")
inline fun Double.clamp01() = this.clamp(0.0, 1.0)
@Suppress("NOTHING_TO_INLINE")
inline fun Float.clamp01() = this.clamp(0.0f, 1.0f)