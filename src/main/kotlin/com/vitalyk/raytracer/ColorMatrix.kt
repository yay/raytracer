package com.vitalyk.raytracer

import javafx.scene.canvas.Canvas
import javafx.scene.image.Image
import javafx.scene.image.WritableImage
import java.util.concurrent.Callable
import java.util.concurrent.ForkJoinPool

typealias ColorMatrix = Matrix2D<Color>

fun ColorMatrix(width: Int, height: Int) = Matrix2D(width, height, Color.black)
fun ColorMatrix(width: Int, height: Int, chunks: Int = 0, init: (Int, Int) -> Color) =
    ColorMatrix(width, height).fill(chunks, init)

private val colorMatrixThreadPool = ForkJoinPool()

fun ColorMatrix.fill(chunks: Int = 0, init: (Int, Int) -> Color): ColorMatrix {
    val (width, height) = this.size()

    if (chunks > 1) {
        val bandwidth = Math.floor(width.toDouble() / chunks).toInt()

        val k = chunks - 1
        val map = (0..k).map { i ->
            colorMatrixThreadPool.submit(Callable {
                val x1 = i * bandwidth
                fillRegion(x1, 0, if (i == k) width else x1 + bandwidth, height, init)
            })
        }
        map.forEach { it.get() }
    } else {
        fillRegion(0, 0, width, height, init)
    }

    return this
}

inline fun ColorMatrix.fillRegion(x1: Int, y1: Int, x2: Int, y2: Int,
                                  init: (Int, Int) -> Color): ColorMatrix {
    for (x in x1 until x2) {
        for (y in y1 until y2) {
            this[x][y] = init(x, y)
        }
    }
    return this
}

fun ColorMatrix.fillCanvas(canvas: Canvas, scale: Boolean = false) {
    val (width, height) = this.size()
    if (canvas.width < width || canvas.height < height) return
    val writer = canvas.graphicsContext2D.pixelWriter

    if (scale) {
        // Nearest neighbor scaling.
        val sx = width.toDouble() / canvas.width
        val sy = height.toDouble() / canvas.height
        for (x in 0 until canvas.width.toInt()) {
            for (y in 0 until canvas.height.toInt()) {
                val mx = (x * sx).toInt()
                val my = (y * sy).toInt()
                writer.setColor(x, y, this[mx][my].toFxColor())
            }
        }
    } else {
        for (x in 0 until width) {
            for (y in 0 until height) {
                writer.setColor(x, y, this[x][y].toFxColor())
            }
        }
    }
}

fun ColorMatrix.recolor(fn: (Color) -> Color): ColorMatrix {
    val size = this.size()

    return Matrix2D(size.width, size.height) { x, y -> fn(this[x][y]) }
}

fun ColorMatrix.toImage(): WritableImage {
    val size = size()
    val image = WritableImage(size.width, size.height)
    val pw = image.pixelWriter

    for (x in 0 until size.width) {
        for (y in 0 until size.height) {
            pw.setColor(x, y, this[x][y].toFxColor())
        }
    }

    return image
}

fun ColorMatrix.toCanvas(): Canvas = this.toImage().toCanvas()

fun Image.toColorMatrix(linear: Boolean = false): ColorMatrix {
    val reader = this.pixelReader
    val w = this.width.toInt()
    val h = this.height.toInt()

    return if (linear) {
        Matrix2D(w, h) { x, y -> reader.getColor(x, y).toColor().linear() }
    } else {
        Matrix2D(w, h) { x, y -> reader.getColor(x, y).toColor() }
    }
}