package com.vitalyk.raytracer

class Ray (
    var origin: Vector3,
    direction: Vector3
) {
    var direction = direction.normalize()
        set(value) { field = value.normalize() }
}