package com.vitalyk.raytracer

data class Color (
    val r: Float = 0.0f,
    val g: Float = 0.0f,
    val b: Float = 0.0f
) {
    constructor(r: Double, g: Double, b: Double) : this(
        r.toFloat(),
        g.toFloat(),
        b.toFloat()
    )

    companion object {
        val black = Color()
    }

    operator fun plus(other: Color) = Color(r + other.r, g + other.g, b + other.b)
    operator fun plus(other: FxColor) = Color(r + other.red, g + other.green, b + other.blue)

    operator fun times(k: Double) = times(k.toFloat())
    operator fun times(k: Float) = Color(r * k, g * k, b * k)

    operator fun times(other: Color) = Color(
        r * other.r,
        g * other.g,
        b * other.b
    )

    fun clamp(): Color = Color(
        r.clamp01(),
        g.clamp01(),
        b.clamp01()
    )

    fun pow(pow: Double) = Color(
        r.toDouble().pow(pow).toFloat(),
        g.toDouble().pow(pow).toFloat(),
        b.toDouble().pow(pow).toFloat()
    )

    private fun linear_to_sRGB_component(c: Float): Float {
        return if (c <= 0.0031308f)
            12.92f * c
        else
            1.055f * c.toDouble().pow(1/2.4).toFloat() - 0.055f
    }

    // Fast approximation of pow().
    // Color accuracy suffers when this is used in `linear_to_sRGB_component`,
    // the gradients are not smooth.
    fun pow(a: Double, b: Double): Double {
        val x = (java.lang.Double.doubleToLongBits(a) shr 32).toInt()
        val y = (b * (x - 1072632447) + 1072632447).toInt()
        return java.lang.Double.longBitsToDouble(y.toLong() shl 32)
    }

    private fun sRGB_to_linear_component(c: Float): Float {
        return if (c <= 0.04045f)
            c / 12.92f
        else
            ((c + 0.055f) / 1.055).pow(2.4).toFloat()
    }

    fun sRGB(): Color = Color(
        linear_to_sRGB_component(r),
        linear_to_sRGB_component(g),
        linear_to_sRGB_component(b)
    )

    fun linear(): Color = Color(
        sRGB_to_linear_component(r),
        sRGB_to_linear_component(g),
        sRGB_to_linear_component(b)
    )

    fun toFxColor(): FxColor = FxColor(
        r.toDouble(),
        g.toDouble(),
        b.toDouble(),
        1.0
    )
}

/**
 * Careful, no bounds check. Use 'clamp01' (if needed) before calling this.
 */
fun Double.toColor() = this.toFloat().toColor()

/**
 * Careful, no bounds check. Use 'clamp01' (if needed) before calling this.
 */
fun Float.toColor() = Color(this, this, this)

/**
 * Average the most prominent and least prominent colors.
 */
fun Color.getLightness() = (r.max(g).max(b) + r.min(g).min(b)) / 2.0

fun Color.getAverage() = (r + g + b) / 3.0

/**
 * Get a weighted average of individual color components
 * to account for human perception (we are more sensitive
 * to green than other colors).
 */
fun Color.getLuma() = 0.21 * r + 0.72 * g + 0.07 * b