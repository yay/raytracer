package com.vitalyk.raytracer

class Texture(val image: ColorMatrix) {
    val size = image.size()
    val width = size.width
    val height = size.height

    val maxX = width - 1
    val maxY = height - 1

    var scaleX: Double
    var scaleY: Double

    // Texture translation in world coordinates.
    var translateX: Double = 0.0
    var translateY: Double = 0.0

    init {
        scaleX = width.toDouble() / height
        scaleY = 1.0
    }

    fun scale(sx: Double, sy: Double = sx) {
        scaleX *= sx
        scaleY *= sy
    }

    @Suppress("NOTHING_TO_INLINE")
    inline fun Double.wrap(length: Int, scale: Double): Int {
        val absScale = Math.abs(scale)

        var x = if (this < 0)
            // Prevent the texture from flipping at the origin.
            absScale - (-this % absScale)
        else
            this % absScale

        // Flip the texture everywhere, if the scale is negative.
        if (scale < 0) {
            x = absScale - x
        }

        return (x / absScale * length).toInt()
    }

    fun colorAt(uv: UV): Color {
        val x = (uv.u + translateX).wrap(maxX, scaleX)
        val y = (uv.v + translateY).wrap(maxY, scaleY)

        return image[x][y]
    }
}