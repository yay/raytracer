package com.vitalyk.raytracer.math

// Icecore's atan2
// http://www.java-gaming.org/topics/extremely-fast-atan2/36467/msg/346145/view.html#msg346145

object Icecore {

    private val Size_Ac = 100000
    private val Size_Ar = Size_Ac + 1
    private val Pi = Math.PI.toFloat()
    private val Pi_H = Pi / 2

    private val Atan2 = FloatArray(Size_Ar)
    private val Atan2_PM = FloatArray(Size_Ar)
    private val Atan2_MP = FloatArray(Size_Ar)
    private val Atan2_MM = FloatArray(Size_Ar)

    private val Atan2_R = FloatArray(Size_Ar)
    private val Atan2_RPM = FloatArray(Size_Ar)
    private val Atan2_RMP = FloatArray(Size_Ar)
    private val Atan2_RMM = FloatArray(Size_Ar)

    init {
        for (i in 0..Size_Ac) {
            val d = i.toDouble() / Size_Ac
            val x = 1.0
            val y = x * d
            val v = Math.atan2(y, x).toFloat()

            Atan2[i] = v
            Atan2_PM[i] = Pi - v
            Atan2_MP[i] = -v
            Atan2_MM[i] = -Pi + v

            Atan2_R[i] = Pi_H - v
            Atan2_RPM[i] = Pi_H + v
            Atan2_RMP[i] = -Pi_H + v
            Atan2_RMM[i] = -Pi_H - v
        }
    }

    @Suppress("NOTHING_TO_INLINE")
    inline fun atan2(y: Double, x: Double) = atan2(x.toFloat(), y.toFloat())

    fun atan2(y: Float, x: Float): Float {
        var y = y
        var x = x

        if (y < 0) {
            return if (x < 0) {
                // (y < x) because == (-y > -x)
                if (y < x) {
                    Atan2_RMM[(x / y * Size_Ac).toInt()]
                } else {
                    Atan2_MM[(y / x * Size_Ac).toInt()]
                }
            } else {
                y = -y
                if (y > x) {
                    Atan2_RMP[(x / y * Size_Ac).toInt()]
                } else {
                    Atan2_MP[(y / x * Size_Ac).toInt()]
                }
            }
        } else {
            return if (x < 0) {
                x = -x
                if (y > x) {
                    Atan2_RPM[(x / y * Size_Ac).toInt()]
                } else {
                    Atan2_PM[(y / x * Size_Ac).toInt()]
                }
            } else {
                if (y > x) {
                    Atan2_R[(x / y * Size_Ac).toInt()]
                } else {
                    Atan2[(y / x * Size_Ac).toInt()]
                }
            }
        }
    }
}