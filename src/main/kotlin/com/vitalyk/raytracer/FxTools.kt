package com.vitalyk.raytracer

import javafx.embed.swing.SwingFXUtils
import javafx.scene.canvas.Canvas
import javafx.scene.image.Image
import javafx.scene.image.WritableImage
import javafx.stage.FileChooser
import javafx.stage.Window
import javax.imageio.ImageIO

typealias FxColor = javafx.scene.paint.Color

fun Image.toCanvas(): Canvas {
    val canvas = Canvas(width, height)
    canvas.graphicsContext2D.drawImage(this, 0.0, 0.0)
    return canvas
}

fun Image.toCanvas(scale: Int = 1): Canvas {
    val w = width * scale
    val h = height * scale
    val canvas = Canvas(w, h)
    canvas.graphicsContext2D.drawImage(this, 0.0, 0.0, w, h)
    return canvas
}

fun Canvas(width: Int, height: Int) =
    Canvas(width.toDouble(), height.toDouble())

fun Canvas.drawImage(image: Image) {
    this.graphicsContext2D.drawImage(image, 0.0, 0.0, width, height)
}

fun Image.saveDialog(
    owner: Window? = null,
    title: String = "Save image",
    fileName: String = "Image.png",
    format: String = "png"
) {
    val fileChooser = FileChooser()
    fileChooser.title = title
    fileChooser.initialFileName = fileName

    val file = fileChooser.showSaveDialog(owner)

    if (file != null) {
        ImageIO.write(
            SwingFXUtils.fromFXImage(this, null),
            format,
            file
        )
    }
}

fun loadImageDialog(
    owner: Window? = null,
    title: String = "Load image"
): Image? {
    val fileChooser = FileChooser()
    fileChooser.title = title
    fileChooser.extensionFilters.add(
        FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg")
    )
    val file = fileChooser.showOpenDialog(owner)

    if (file != null) {
        return Image("file:" + file.absolutePath)
    }

    return null
}

fun FxColor.toColor() = Color(red, green, blue)

operator fun FxColor.plus(other: FxColor) =
    Color(red + other.red, green + other.green, blue + other.blue)

operator fun FxColor.plus(other: Color) =
    Color(red + other.r, green + other.g, blue + other.b)

fun Matrix2D<Double>.toImage(): WritableImage {
    val size = size()
    val image = WritableImage(size.width, size.height)
    val pw = image.pixelWriter

    for (x in 0 until size.width) {
        for (y in 0 until size.height) {
            val value = this[x][y]
            pw.setColor(x, y, FxColor(value, value, value, 1.0))
        }
    }

    return image
}