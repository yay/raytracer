package com.vitalyk.raytracer

/**
 * Normalized texture space (u, v) ranging from 0 to 1.
 */
data class UV(
    val u: Double = 0.0,
    val v: Double = 0.0
)