package com.vitalyk.raytracer

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import javafx.stage.FileChooser
import javafx.stage.Window
import java.io.File

/**
 * A singleton for loading and saving settings objects.
 */
object Settings {

    private val mapper by lazy { jacksonObjectMapper() }

    /**
     * Creates a filename for the given settings object based on its class name.
     */
    private fun getFileName(obj: Any): String = "${obj::class.java.name.split(".").last()}.json"

    fun load(obj: Any, filename: String = getFileName(obj)) {
        mapper.readValue(File(filename), obj::class.java)
    }

    fun save(obj: Any, filename: String = getFileName(obj)) {
        mapper.writerWithDefaultPrettyPrinter().writeValue(File(filename), obj)
    }

    fun saveDialog(obj: Any, filename: String = getFileName(obj),
                   owner: Window? = null, title: String? = null) {

        val fileChooser = FileChooser()
        fileChooser.title = title ?: "Save file"
        fileChooser.initialFileName = filename

        val file = fileChooser.showSaveDialog(owner)

        if (file != null) {
            mapper.writerWithDefaultPrettyPrinter().writeValue(file, obj)
        }
    }

    fun loadDialog(obj: Any, filters: Map<String, List<String>>, owner: Window? = null, title: String? = null) {

        val fileChooser = FileChooser()
        fileChooser.title = title ?: "Load file"
        for ((description, extensions) in filters) {
            fileChooser.extensionFilters.add(
                FileChooser.ExtensionFilter(description, extensions)
            )
        }

        val file = fileChooser.showOpenDialog(owner)

        if (file != null) {
            mapper.readValue(file, obj::class.java)
        }
    }
}