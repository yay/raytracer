package com.vitalyk.raytracer

/**
 * https://www.quora.com/Floating-Point-How-does-Fused-Multiply-Add-FMA-work-and-what-is-its-importance-in-computing
 */

/**
 * --------------------------------------
 *              Terminology
 * --------------------------------------
 * A low-pass filter is a filter that passes signals with a frequency lower
 * than a certain cutoff frequency and attenuates signals with frequencies higher
 * than the cutoff frequency.
 */

fun getImageLuma(image: ColorMatrix): Matrix2D<Double> {
    val size = image.size()

    return Matrix2D(size.width, size.height) { x, y -> image[x][y].getLuma() }
}

/**
 * The minimum amount of local contrast required to apply algorithm.
 * 1/3 – too little
 * 1/4 – low quality
 * 1/8 – high quality
 * 1/16 – overkill
 */
val fxaaEdgeThreshold: Double = 1.0 / 8.0

/**
 * Trims the algorithm from processing darks.
 * 1/32 – visible limit
 * 1/16 – high quality
 * 1/12 – upper limit (start of visible unfiltered edges)
 */
val fxaaEdgeThresholdMin: Double = 1.0 / 12.0

/**
 * Toggle sub-pixel filtering.
 * 0 – turn off
 * 1 – turn on
 * 2 – turn on full (ignore `fxaaSubpixTrim` and `fxaaSubpixCap`)
 */
val fxaaSubpix: Int = 1

/**
 * Controls removal of sub-pixel aliasing.
 * 1/2 – low removal
 * 1/3 – medium removal
 * 1/4 – default removal
 * 1/8 – high removal
 * 0 – complete removal
 */
val fxaaSubpixTrim: Double = 1.0 / 4.0

val fxaaSubpixTrimScale: Double = 1.0 / (1.0 - fxaaSubpixTrim)

/**
 * Insures fine detail is not completely removed.
 * This partly overrides `fxaaSubpixTrim`.
 * 3/4 – default amount of filtering
 * 7/8 – high amount of filtering
 * 1 – no capping of filtering
 */
val fxaaSubpixCap: Double = 3.0 / 4.0

/**
 * Controls the maximum number of search steps.
 * Multiply by `fxaaSearchAcceleration` for filtering radius.
 */
val fxaaSearchSteps: Int = 5


//val fxaaSearchAcceleration = 2

enum class FxaaDebug {
    None,
    Passthrough, // Show edges in red.
    HorzVert,    // Show horizontal edges in gold and vertical in blue.
    Pair,
    NegPos,
    Offset
}

fun fxaaImage(image: ColorMatrix, debug: FxaaDebug = FxaaDebug.None): ColorMatrix {
    val (width, height) = image.size()

    val GOLD = FxColor.GOLD.toColor()
    val BLUE = FxColor.BLUE.toColor()

    val luma = getImageLuma(image)

    for (x in 1..width-2) {
        for (y in 1..height-2) {

        /*----------------------------------------------------------------------------
            EARLY EXIT IF LOCAL CONTRAST BELOW EDGE DETECT LIMIT
        ------------------------------------------------------------------------------
            Majority of pixels of a typical image do not require filtering,
            often pixels are grouped into blocks which could benefit from early exit
            right at the beginning of the algorithm.
            Given the following neighborhood,

                  N
                W M E
                  S

            If the difference in local maximum and minimum luma (contrast "range")
            is lower than a threshold proportional to the maximum local luma ("rangeMax"),
            then the algorithm early exits (no visible aliasing).
            This threshold is clamped at a minimum value ("fxaaEdgeThresholdMin")
            to avoid processing in really dark areas.
        ----------------------------------------------------------------------------*/

            val lumaM = luma[x][y]
            val lumaN = luma[x][y-1]
            val lumaE = luma[x+1][y]
            val lumaS = luma[x][y+1]
            val lumaW = luma[x-1][y]

            val rangeMin = lumaM.min(lumaN).min(lumaE).min(lumaS).min(lumaW)
            val rangeMax = lumaM.max(lumaN).max(lumaE).max(lumaS).max(lumaW)
            val range = rangeMax - rangeMin  // local contrast

            if (range < (rangeMax * fxaaEdgeThreshold).clampMin(fxaaEdgeThresholdMin))
                continue  // not an edge

        /*----------------------------------------------------------------------------
                               COMPUTE LOW-PASS
        ------------------------------------------------------------------------------
            FXAA computes a local neighborhood low-pass value as follows,

              (N + W + E + S) / 4

            The ratio of pixel contrast to local contrast is used to detect
            sub-pixel aliasing. This ratio approaches 1.0 in the presence of
            single pixel dots and otherwise begins to fall off towards 0.0
            as more pixels contribute to an edge. The ratio is transformed
            into the amount of low-pass filter to blend in at the end of the
            algorithm.
        ----------------------------------------------------------------------------*/

            val lumaL = (lumaN + lumaE + lumaS + lumaW) * 0.25  // low-pass luma
            val rangeL = Math.abs(lumaL - lumaM)  // pixel contrast

            val blendL = when (fxaaSubpix) {
                1 -> ((rangeL / range - fxaaSubpixTrim).clampMin(0.0) * fxaaSubpixTrimScale)
                    .clampMax(fxaaSubpixCap)
                2 -> rangeL / range
                else -> 0.0
            }

            if (debug == FxaaDebug.Passthrough) {
                image[x][y] = Color(1.0, blendL / fxaaSubpixCap, 0.0)
            }

        /*----------------------------------------------------------------------------
                CHOOSE VERTICAL OR HORIZONTAL SEARCH
        ------------------------------------------------------------------------------
            FXAA uses the following local neighborhood,

                NW N NE
                W  M  E
                SW S SE

            To compute an edge amount for both vertical and horizontal directions.
            FXAA takes the weighted average magnitude of the high-pass values
            for rows and columns as an indication of local edge amount.

            A low-pass value for anti-sub-pixel-aliasing is computed as
                (N + W + E + S + M + NW + NE + SW + SE) / 9
            This full box pattern has higher quality than other options.

            A single loop searches in both the negative and positive direction
            in parallel on the chosen horizontal or vertical orientation.
        ----------------------------------------------------------------------------*/

            val rgbM = image[x][y]
            val rgbN = image[x][y-1]
            val rgbE = image[x+1][y]
            val rgbS = image[x][y+1]
            val rgbW = image[x-1][y]

            val rgbNE = image[x+1][y-1]
            val rgbSE = image[x+1][y+1]
            val rgbSW = image[x-1][y+1]
            val rgbNW = image[x-1][y-1]

            var rgbL = rgbM + rgbN + rgbE + rgbS + rgbW
            rgbL += rgbNW + rgbNE + rgbSW + rgbSE
            rgbL *= 1.0 / 9.0

            val lumaNE = luma[x+1][y-1]
            val lumaSE = luma[x+1][y+1]
            val lumaSW = luma[x-1][y+1]
            val lumaNW = luma[x-1][y-1]

            val edgeVert = Math.abs(0.25 * lumaNW + -0.5 * lumaN + 0.25 * lumaNE) +
                           Math.abs(0.50 * lumaW  + -1.0 * lumaM + 0.50 * lumaE ) +
                           Math.abs(0.25 * lumaSW + -0.5 * lumaS + 0.25 * lumaSE)

            val edgeHorz = Math.abs(0.25 * lumaNW + -0.5 * lumaW + 0.25 * lumaSW) +
                           Math.abs(0.50 * lumaN  + -1.0 * lumaM + 0.50 * lumaS ) +
                           Math.abs(0.25 * lumaNE + -0.5 * lumaE + 0.25 * lumaSE)

            val horzSpan = edgeHorz >= edgeVert

            // Pair the pixel with its highest contrast neighbor 90 degrees to the
            // local edge direction and search along the edge in the positive and
            // negative directions until a search limit is reached or the average
            // luminance of the pair moving along the edge changes by enough
            // to signify end-of-edge.
            if (horzSpan) {
                if (debug == FxaaDebug.HorzVert) {
                    image[x][y] = GOLD
                }
                for (i in 0 until fxaaSearchSteps) {

                }
            } else {
                if (debug == FxaaDebug.HorzVert) {
                    image[x][y] = BLUE
                }
            }

        }
    }

    return image
}