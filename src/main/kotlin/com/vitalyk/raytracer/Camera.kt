package com.vitalyk.raytracer

class Camera(
    width: Int = 800,
    height: Int = 600,
    fov: Double = 90.0,
    var origin: Vector3 = Vector3(),
    var direction: Vector3 = Vector3(0.0, 0.0, -1.0)
) {
    var up = Vector3(0.0, 1.0, 0.0)
        set(value) {
            field = value.normalize()
        }

    private val shadowBias: Double = 1e-12
    val maxRecursionDepth: Int = 5

    var width: Int = width
        set(value) {
            field = value
            updateAspectRatio()
        }

    var height: Int = height
        set(value) {
            field = value
            updateAspectRatio()
        }

    var aspectRatio: Double = updateAspectRatio()
        private set(value) { field = value }

    private fun updateAspectRatio(): Double {
        val ratio = width.toDouble() / height
        aspectRatio = ratio
        return ratio
    }

    var fov: Double = fov
        set(value) {
            field = value
            updateFovAdjustment()
        }

    /**
     * Sets "optimal" FOV for the current aspect ratio.
     */
    fun updateFov() {
        fov = 90.0 * aspectRatio / (4.0 / 3)
    }

    // The axis of the sensor plane.
    lateinit private var xAxis: Vector3
    lateinit private var yAxis: Vector3

    // We’ll be by pretending there’s a two-unit by two-unit square
    // one unit in front of the camera. This square represents
    // the image sensor  or film of our camera.
    // We need to translate the (0…scene.width, 0…scene.height) coordinates
    // of our pixels to the (-1.0…1.0, -1.0…1.0) coordinates of the sensor.
    // If we didn’t use the aspect ratio, the rays would be closer together
    // in one dimension than the other dimension.
    private val sensorRangeX = (-1.0).rangeTo(1.0)
    private val sensorRangeY = (-1.0).rangeTo(1.0)
    private val sensorSpanX = sensorRangeX.endInclusive - sensorRangeX.start
    private val sensorSpanY = sensorRangeY.endInclusive - sensorRangeY.start

    private var fovAdjustment = updateFovAdjustment()
    private fun updateFovAdjustment(): Double {
        val value = Math.tan(fov.toRadians() / sensorSpanX)
        fovAdjustment = value
        return value
    }

    fun createPrime(x: Double, y: Double): Ray {
        // Construct a coordinate system in the camera's origin plane.
        yAxis = up
        xAxis = direction.cross(yAxis).normalize()

        // Normalized screen coordinates relative to top-left corner.
        val nx = (x + 0.5) / width   // [0, 1]
        val ny = (y + 0.5) / height  // [0, 1]

        // 2D sensor plane coordinates relative to (0, 0).
        val sx = (sensorRangeX.start + nx * sensorSpanX) * aspectRatio * fovAdjustment
        val sy = sensorRangeY.start + ny * sensorSpanY

        // 3D sensor plane coordinates relative to camera origin.
        val vx = xAxis * sx
        val vy = yAxis * sy

        // Subtract `vy` because scene and screen Y-axes go in opposite directions.
        return Ray(
            origin = origin,
            direction = vx - vy + direction
        )
    }

    fun renderScene(scene: Scene, matrix: ColorMatrix) {
        matrix.fill(chunks = 8) { x, y ->

            val primeRay = createPrime(x.toDouble(), y.toDouble())
            val intersection = scene.trace(primeRay)

            var color = Color.black

            if (intersection != null) {
                val target = intersection.target
                val hitPoint = primeRay.origin + (primeRay.direction * intersection.distance)
                val hitNormal = target.normal(hitPoint)

                for (light in scene.lights) {
                    val directionToLight = light.directionToLight(hitPoint)

                    val shadowRay = Ray(
                        // Sometimes, the hit point will be ever so slightly inside the
                        // intersected object, and so the shadow ray will intersect with
                        // the same object the prime ray did (lookup 'shadow acne').
                        // So to prevent this from happening we nudge the shadow ray
                        // origin slightly in the direction of the normal.
                        origin = hitPoint + hitNormal * shadowBias,
                        direction = directionToLight
                    )
                    val shadowIntersection = scene.trace(shadowRay)
                    val isLit = shadowIntersection == null ||
                        shadowIntersection.distance > light.distanceToPoint(hitPoint)
                    val lightIntensity = if (isLit) light.intensityAtPoint(hitPoint) else 0.0

                    // The amount of light that lands on this point is proportional
                    // to the cosine of the angle between the surface normal and
                    // the direction to the light (Lambert’s Cosine Law). The dot
                    // product is |a| x [b| * cos(θ), but because we use normalized
                    // vectors their lengths will be 1.
                    val lightPower = hitNormal.dot(directionToLight).clampMin(0.0) * lightIntensity
                    // Why divide by PI?
                    // https://www.scratchapixel.com/lessons/3d-basic-rendering/introduction-to-shading/diffuse-lambertian-shading
                    val lightReflected = target.material.albedo / Math.PI
                    val uv = if (target.material.texture != null) target.uv(hitPoint) else null

                    color += (target.material.color(uv) * light.color * lightPower * lightReflected)
                }
                color.clamp().sRGB()
            } else {
                color
            }

        }
    }

}