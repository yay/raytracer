package com.vitalyk.raytracer

import com.vitalyk.raytracer.light.Directional
import com.vitalyk.raytracer.light.Spherical
import com.vitalyk.raytracer.shape.Plane
import com.vitalyk.raytracer.shape.Sphere
import javafx.scene.image.Image

/**
 * Since we use JavaFx images for textures, a scene has to be created after
 * JavaFx has initialized (or we'll get the "Internal graphics not initialized yet"
 * error), so we wrap scene creation in a function and call it when appropriate.
 */
fun getTestScene(): Scene {
    val checkerboard = Image("/img/checkerboard.png").toColorMatrix(linear = true)

    return Scene(
        camera = Camera(
            width = 800,
            height = 600,
            fov = 90.0,
            origin = Vector3(0.0, 2.0, 0.0)
        ),
        items = listOf(
            Sphere(
                center = Vector3(
                    x = -2.0,
                    y = 1.0,
                    z = -5.5
                ),
                radius = 1.0,
                material = Material(
                    color = Color(1.0, 0.0, 0.0),
                    albedo = 0.28f
                )
            ),
            Sphere(
                center = Vector3(
                    x = 0.0,
                    y = 3.0,
                    z = -2.5
                ),
                radius = 0.5,
                material = Material(
                    color = Color(0.12, 0.56, 1.00),
                    albedo = 0.18f
                )
            ),
            Sphere(
                center = Vector3(
                    x = 3.9,
                    y = 2.0,
                    z = -6.0
                ),
                radius = 2.0,
                material = Material(
                    color = Color(0.0, 1.0, 0.0),
                    albedo = 0.18f
                )
            ),
            Plane(
                origin = Vector3(
                    x = 0.0,
                    y = 0.0,
                    z = -5.0
                ),
                normal = Vector3(
                    x = 0.0,
                    y = -1.0,
                    z = 0.0
                ),
                material = Material(
                    texture = Texture(checkerboard),
                    albedo = 0.38f
                )
            ),
            Plane(
                origin = Vector3(
                    x = 0.0,
                    y = 0.0,
                    z = -25.0
                ),
                normal = Vector3(
                    x = 0.0,
                    y = 0.0,
                    z = -1.0
                ),
                material = Material(
                    texture = Texture(Image("/img/sunset.jpg").toColorMatrix(linear = true)).apply {
                        translateX = 35.0
                        translateY = -20.0
                        this.scale(50.0)
                    },
                    albedo = 0.5f
                )
            )
        ),
        lights = listOf(
//            Directional(
//                color = Color(1.0, 1.0, 1.0),
//                intensity = 2.0,
//                direction = Vector3(
//                    x = -1.0,
//                    y = -1.0,
//                    z = -2.0
//                )
//            ),
            Directional(
                color = FxColor.WHITE.toColor(),
                intensity = 8.0,
                direction = Vector3(
                    x = 2.5,
                    y = -0.5,
                    z = -2.0
                )
            ),
            Spherical(
                color = Color(1.0, 1.0, 1.0),
                intensity = 200.0,
                position = Vector3(
                    x = 1.0,
                    y = 1.0,
                    z = -4.5
                )
            )
        )
    )
}